<?php
set_error_handler(function($no, $str, $file, $line ) { throw new ErrorException($str, 0, $no, $file, $line); });
try{
    $ts = strtotime($_GET['time']) or ($ts = time());
}catch(Exception $e){
    $ts = time();
}
$time = array();
$day  = date('N', $ts);

if (in_array($day, array(3, 6)) && ($today = strtotime('today 8pm', $ts)) > $ts) {
    $time[] = $today;
}

$time[] = strtotime('next Wednesday 8pm', $ts);
$time[] = strtotime('next saturday 8pm', $ts);
?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Place favicon.ico in the root directory -->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3 col-md-6 col-md-offset-3">
            <form class="form" action="" method="get">
                <div class="form-group">
                    <label class="control-label">Select a date below</label>
                    <div class="input-group ">
                        <input type="datetime-local" class="form-control input-sm" placeholder="Pick a date" name="time" value="<?=date('Y-m-d\TH:i:s', $ts)?>"/>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-success"><i
                                    class="glyphicon glyphicon-circle-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="text-center">
        <span class="label label-info">Next draw: <strong><?=date('l, jS \of F Y h:i A', min($time));?></strong></span>
    </div>
</body>
</html>
